import './App.css';
import * as React from 'react'
import { Routes, Route } from "react-router-dom";
import NotFound from './components/404';
import Register from './components/Register';
import HomePage from './components/HomePage';
import Login from './components/Login';
import Navbar from './components/Navbar';
import { connect } from 'react-redux';
import { Component } from 'react';

class App extends Component {

  render() {
    return (
      <div className="App">
        <Navbar></Navbar>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </div>
    );
  }
}

// ����������� ������ �� store
const mapStateToProps = (store) => {
  console.log(store); // ���������, ��� �� � ��� � store?
  return {
    user: store.user,
  };
};

// � ��� ��������� App, � ������� connect(mapStateToProps)
export default connect(mapStateToProps)(App);
